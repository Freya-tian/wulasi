const express = require ('express');
const os = require('os')
const cluster = require('cluster')
const fs = require('fs')
const PORT = process.PORT || 5001


const clusterWorkerSize = os.cpus().length 
if(clusterWorkerSize > 1){
    if (cluster.isMaster) { 
        for (let i=0; i < clusterWorkerSize/2; i++) { 
          cluster.fork() 
        } 
        cluster.on("exit", function(worker) { 
          console.log("Worker", worker.id, " has exitted.") 
        }) 
      } else { 
        const app = express() 
        app.get('/user',(req,res)=>{
            fs.readFile("./data.json",'utf-8', function(err,datas){
                if(err){
                    res.send(err);
                }
                    const user = JSON.parse(datas.toString());
        
                    res.send(user) ;
                
            })    
        })
        app.listen(PORT, function () { 
          console.log(`Express server listening on port ${PORT} and worker ${process.pid}`) 
        }) 
      } 
} else { 
    const app = express() 
    app.get('/user',(req,res)=>{
        fs.readFile("./data.json",'utf-8', function(err,datas){
            if(err){
                res.send(err);
            }
                const user = JSON.parse(datas.toString());
    
                res.send(user) ;
            
        })    
    })
    app.listen(PORT, function () { 
      console.log(`Express server listening on port ${PORT} with the single worker ${process.pid}`) 
    }) 
  } 